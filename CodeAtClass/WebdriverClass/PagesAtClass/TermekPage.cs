﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.WidgetsAtClass;

namespace WebdriverClass.PagesAtClass
{
    class TermekPage : BasePage
    {
        public TermekPage(IWebDriver webDriver) : base(webDriver)
        {

        }
        public static TermekPage navigate(IWebDriver webDriver)
        {

            return new TermekPage(webDriver);
        }
        public TermekWidget GetProductWidget()
        {

            return new TermekWidget(Driver);
        }
    }
}
