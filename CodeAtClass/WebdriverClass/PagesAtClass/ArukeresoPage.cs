﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.WidgetsAtClass;

namespace WebdriverClass.PagesAtClass
{
    class ArukeresoPage : BasePage
    {
        public ArukeresoPage(IWebDriver webDriver) : base(webDriver)
        {
        }
        public static ArukeresoPage navigate(IWebDriver webDriver)
        {

            webDriver.Navigate().GoToUrl("https://www.arukereso.hu");

            return new ArukeresoPage(webDriver);
        }
        public ArukeresoWidget GetArukeresoWidget()
        {

            return new ArukeresoWidget(Driver);
        }
    }
}
