﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Chrome;

using OpenQA.Selenium.Support.UI;
using System.Collections.ObjectModel;

namespace WebdriverClass
{
    class WebdriverWindowTestAtClass : TestBase
    {
        [Test]
        public void ResponsiveWindow()
        {
            Driver.Navigate().GoToUrl("http://www.expedia.com/");
            //Maximize browser window
            Assert.IsTrue(Driver.FindElement(By.Id("header-account-menu")).Displayed);
            Assert.IsFalse(Driver.FindElement(By.Id("header-mobile-toggle")).Displayed);
            //Set browser window size to 600x600
            Assert.IsTrue(Driver.FindElement(By.Id("header-mobile-toggle")).Displayed);
            Assert.IsFalse(Driver.FindElement(By.Id("header-account-menu")).Displayed);
        }

        [Test]
        public void MultipleWindow()
        {
            // have to use Chrome because of SHIFT+Click, so Driver is overwritten
            Driver = new ChromeDriver();
            Driver.Navigate().GoToUrl("https://www.amazon.com/gp/gw/ajax/s.html");
            // String mainWindow <= save current window's handle in this string
            new Actions(Driver).KeyDown(Keys.Shift).Click(Driver.FindElement(By.CssSelector("a[href*='cart']"))).KeyUp(Keys.Shift).Perform();
            // ReadOnlyCollection<string> windows <= save all window handles here
            // Switch to last opened window
            StringAssert.Contains("Cart", Driver.Title);
            // Close active window
            // Switch to our main window
            Assert.IsTrue(Driver.FindElement(By.Id("gw-card-layout")).Displayed);
        }


    }
}
