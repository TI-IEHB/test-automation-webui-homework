﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;

namespace WebdriverClass
{
    class TypingTestAtClass : TestBase
    {
        [Test]
        public void TypeExample()
        {
            Driver.Navigate().GoToUrl("http://www.google.com");
            IWebElement searchField = Driver.FindElement(By.Name("q"));
            
            // Type selenium word to google search field
            Assert.AreEqual("Selenium", searchField.GetAttribute("value"));
            
            // Clear google search field
            Assert.IsEmpty(searchField.GetAttribute("value"));
        }
    }
}
