﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.WidgetsAtClass
{
    class TermekWidget : BasePage
    {
        public TermekWidget(IWebDriver driver) : base(driver)
        {
        }
        public enum FilterOption
        {
            Népszerűek, Olcsók, Akciós, Drágák, Értékelés, Név
        }
        public string ClickResult()
        {
            System.Threading.Thread.Sleep(2000);
            return Driver.FindElement(By.XPath("//*[@id='micro-data']/div[1]/div/h1")).Text;
            
        }
        public string SearchResult()
        {
            System.Threading.Thread.Sleep(2000);
            return Driver.FindElement(By.XPath("//*[@id='micro-data']/div[1]/a")).Text;
        }
    }
}
