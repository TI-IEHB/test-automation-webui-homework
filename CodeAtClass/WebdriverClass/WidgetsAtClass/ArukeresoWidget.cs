﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.WidgetsAtClass
{
    class ArukeresoWidget : BasePage
    {
        public ArukeresoWidget(IWebDriver driver) : base(driver)
        {
        }
        public void Click(string adat)
        {
            Driver.Manage().Window.Maximize();
            System.Threading.Thread.Sleep(2000);
            Driver.FindElement(By.LinkText(adat)).Click();

        }
        public void Search(string adat)
        {
            Driver.Manage().Window.Maximize();
            Driver.FindElement(By.Id("st")).SendKeys(adat);
            Driver.FindElement(By.XPath("//*[@id='aksearch']/div[1]/span/a")).Click();
        }
    }
}
