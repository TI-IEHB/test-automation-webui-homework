﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using WebdriverClass.PagesAtClass;
using WebdriverClass.WidgetsAtClass;

namespace WebdriverClass
{
    class BeadandoPageObjectTest : TestBase
    {
        public object ProductPage { get; private set; }

        [Test, TestCaseSource("Termek")]
        public void ArukeresoClickTest(string adat)
        {

            var arukeresoPage = ArukeresoPage.navigate(Driver);
            ArukeresoWidget arukeresowidget = arukeresoPage.GetArukeresoWidget();

            arukeresowidget.Click(adat);
            var termekPage = TermekPage.navigate(Driver);
            TermekWidget termekwidget = termekPage.GetProductWidget();


            StringAssert.IsMatch(adat, termekwidget.ClickResult());

        }
        [Test, TestCaseSource("Termek")]
        public void ArukeresoKeresoTest(string adat)
        {

            var arukeresoPage = ArukeresoPage.navigate(Driver);
            ArukeresoWidget arukeresowidget = arukeresoPage.GetArukeresoWidget();

            arukeresowidget.Search(adat);
            var termekPage = TermekPage.navigate(Driver);
            TermekWidget termekwidget = termekPage.GetProductWidget();


            StringAssert.IsMatch(adat, termekwidget.SearchResult());

        }
        static IEnumerable Termek()
        {
            
            var doc = XElement.Load(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory) + "/termekek.xml");
            ;
            return
                from vars in doc.Descendants("testData")
                let dataName = vars.Attribute("elementName").Value
                select new object[] { dataName };

        }
    }
}
